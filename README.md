# [www.cnsdhh.com](https://www.cnsdhh.com)

----------------
* 博客  [blog.cnsdhh.com](https://blog.cnsdhh.com)
* 首页  [www.cnsdhh.com](https://www.cnsdhh.com)
* 搜索  [sss.cnsdhh.com](https://sss.cnsdhh.com)
* 百科  [wiki.cnsdhh.com](https://wiki.cnsdhh.com)
* 下载  [bt.f1f2f3f4.com](https://bt.f1f2f3f4.com)



## 本地git首次连接gitee操作步骤

------------------------------
#### 设置本地git用户名和邮箱

    git config --global user.name "CNSDHH-jn"
    git config --global user.email "cnsdhh@sina.com"

#### 先进入帐户主目录然后生成SSH密钥文件
    cd 
    ssh-keygen.exe -t rsa -C "cnsdhh@sina.com"
    cat ~/.ssh/id_rsa.pub

#### 在gitee上添加本地SSH公钥信息（操作步骤如下）
>gitee -> 设置 -> SSH公钥 -> 填写本地SSH公钥信息

#### 在gitee上新建一个同名仓库（操作步骤如下）
>gitee -> 个人主页 -> 项目 -> 点击 **添加** 按钮 -> 填写仓库信息

#### 测试SSH密钥是否可用
    ssh -T git@gitee.com

#### 本地初始化仓库，使git可管理其目录
    cd cnsdhh/
    git init

#### 将项目根目录里的所有内容添加到版本库中，注意小数点，意为添加根目录下的所有文件和目录
    git add .

#### 将修改提交到仓库中，引号内为本次提交说明信息
    git commit -m "First Uploaded in 2018-08-08 08:00:00"

#### 添加远程仓库地址
    git remote add cnsdhh-gitee https://gitee.com/cnsdhh/cnsdhh.git
    git remote -v

#### 获取远程仓库并与本地仓库合并（如果远程库不为空就必须做这一步，否则后面的提交会失败）
    git pull --rebase cnsdhh-gitee master

#### 拉取远程仓库内容至本地仓库
    git fetch cnsdhh-gitee

#### 将远程主分支合并到本地当前分支
    git merge cnsdhh-gitee/master

#### 推送本地仓库提交内容到远程仓库（此命令首次执行时会弹出对话框要求输入gitee用户名和密码）
    git push -u cnsdhh-gitee master  #首次上传需要 -u 参数，以后就不用了
    git push cnsdhh-gitee master

#### 状态查询

    git status

#### 查询本地提交记录

    git log

#### 最后，当提交更改时提示username或password错误的解决办法

    //错误提示：username 或 password 错误
    remote: Incorrect username or password ( access token )
    fatal: Authentication failed for 'https://gitee.com/cnsdhh/cnsdhh.git/'
    
    //原因分析：这是因为通过 https 连接远程仓库时 username 或 password 输入错误所致，修改成 ssh 协议连接即可解决此类问题
    //语法格式：git remote set-url origin [url]
    git remote set-url cnsdhh-gitee git@gitee.com:cnsdhh/cnsdhh.git


