#!/bin/bash

git add .

git commit -m "latest update in $(date +\%Y-\%m-\%d) $(date +\%H:\%M:\%S)"

n=$(git log --author=CNSDHH-jn --since="2018-01-01" --no-merges | grep -e 'commit [a-zA-Z0-9]*' | wc -l)
echo -e "这是第 $n 次提交"

echo -e "==========================================================================================="
echo -e "=            最新的代码已提交成功，可通过 \e[00;31mgit push|fetch|pull\e[00m 命令进行同步更新            ="
echo -e "==========================================================================================="
