package com.homework;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class HomeWork02 implements Runnable {
	private int sum = 1;
	private int stNum = 0;
	private int wdNum = 0;
//	private Lock l = new ReentrantLock();
	
	
	public int getStNum() {
		return stNum;
	}
	
	public int getWdNum() {
		return wdNum;
	}
	
	@Override
	public void run() {
		while (true) {
			synchronized ("cnsdhh") {
//			l.lock();
				//循环执行，每次 sum 加 1，一直回到 100 为止
				if (sum <= 100) {
					String name = Thread.currentThread().getName();
					System.out.println(name + " 卖出了第 " + sum++ + " 个包");
					
					//统计 实体店 与 网购店 各卖出了多少个
					if (name.equals("实体店")) {
						stNum++;
					} else {
						wdNum++;
					}
					
					//每次循环间隔为 10 毫秒
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					break;
				}
//			l.unlock();
			}
		}
	}
}
