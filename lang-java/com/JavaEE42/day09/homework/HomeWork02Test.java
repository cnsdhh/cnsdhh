package com.homework;

import java.util.ArrayList;

public class HomeWork02Test {
	public static void main(String[] args) {
		
		//创建一个集合，用来存放：子线程
		ArrayList<Thread> threads = new ArrayList<>();
		
		HomeWork02 hw = new HomeWork02();
		Thread thread1 = new Thread(hw, "实体店");
		Thread thread2 = new Thread(hw, "网购店");
		//将子线程添加到集合中
		threads.add(thread1);
		threads.add(thread2);
		thread1.start();
		thread2.start();
		
		for (Thread thread : threads) {
			try {
				//等待所有线程执行完毕
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("实体店总共卖出了：" + hw.getStNum());
		System.out.println("网购店总共卖出了：" + hw.getWdNum());
		
	}
}
