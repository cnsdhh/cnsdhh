package com.cnsdhh.car;

//创建抽象类：Car
public abstract class Car {
	
	//声明属性
	String[] model = {"货车","客车","皮卡"};  //类型
	String[] brand = {"宇通","三一","吉利"};  //品牌
	String[] type = {"小型","中型","大型"};   //大小
	String[] color = {"红色","蓝色","白色","黑色","黄色"};  //颜色
	
	int[] ladenT = {2,4,6,8,10,20,40};  //载货量（吨）
	int[] ladenR = {2,5,7,10,40,100};  //载客量（人）
	
	//声明方法
	public void selectCar() {};
	public void sumPrice() {};

}