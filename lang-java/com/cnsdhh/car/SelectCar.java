package com.cnsdhh.car;

import java.util.Scanner;

public class SelectCar extends Car {
	
	//声明成员变量
	int x;                  //存放输入参数
	String xModel;          //存放车型信息
	String xBrand;          //存放品牌信息
	String xType;           //存放大小信息
	public int xLadenT = 0; //存放载货量（吨）
	public int xLadenR = 0; //存放载客量（人）
	String xColor;          //存放颜色信息
	
	//实现抽象方法
	@Override
	public void selectCar() {
		// TODO 自动生成的方法存根
		
		//打印可选车型
		System.out.print("请选择您要的车型（ ");
		for (int i=0; i<model.length; i++) {
			System.out.print(i+"、"+model[i]+" ");
		}
		System.out.print("）：");
		//输入车型信息
		Scanner input = new Scanner(System.in);
		x = input.nextInt();  //接收Int类型数据
		xModel = model[x];    //将数组单元存放在xModel变量中
		//========================================
		
		//打印可选品牌
		System.out.print("请选择您要的品牌（ ");
		for (int i=0; i<brand.length; i++) {
			System.out.print(i+"、"+brand[i]+" ");
		}
		System.out.print("）：");
		//输入品牌信息
		x = input.nextInt();  //接收Int类型数据
		xBrand = brand[x];    //将数组单元存放在xBrand变量中
		//========================================
		
		//打印可选大小
		System.out.print("请选择您要的大小（ ");
		for (int i=0; i<type.length; i++) {
			System.out.print(i+"、"+type[i]+" ");
		}
		System.out.print("）：");
		//输入大小信息
		x = input.nextInt();  //接收Int类型数据
		xType = type[x];      //将数组单元存放在xType变量中
		//========================================
		
		//打印可选载货量或载客量
		switch (xModel) {
		case "货车":
			switch (xType) {
			case "小型":
				System.out.print("请选择您要的载货量[吨]（ ");
				for (int i=0; i<ladenT.length-5; i++) {
					System.out.print(i+"、"+ladenT[i]+"吨 ");
				}
				System.out.print("）：");
				break;
			case "中型":
				System.out.print("请选择您要的载货量[吨]（ ");
				for (int i=2; i<ladenT.length-2; i++) {
					System.out.print(i+"、"+ladenT[i]+"吨 ");
				}
				System.out.print("）：");
				break;
			case "大型":
				System.out.print("请选择您要的载货量[吨]（ ");
				for (int i=5; i<ladenT.length; i++) {
					System.out.print(i+"、"+ladenT[i]+"吨 ");
				}
				System.out.print("）：");break;
			}
			//输入载货量信息
			x = input.nextInt();
			xLadenT = ladenT[x];
			break;
		case "客车":
			switch (xType) {
			case "小型":
				System.out.print("请选择您要的载客量[人]（ ");
				for (int i=0; i<ladenR.length-4; i++) {
					System.out.print(i+"、"+ladenR[i]+"人 ");
				}
				System.out.print("）：");break;
			case "中型":
				System.out.print("请选择您要的载客量[人]（ ");
				for (int i=2; i<ladenR.length-2; i++) {
					System.out.print(i+"、"+ladenR[i]+"人 ");
				}
				System.out.print("）：");break;
			case "大型":
				System.out.print("请选择您要的载客量[人]（ ");
				for (int i=4; i<ladenR.length; i++) {
					System.out.print(i+"、"+ladenR[i]+"人 ");
				}
				System.out.print("）：");break;
			}
			//输入载客量信息
			x = input.nextInt();
			xLadenR = ladenR[x];
			break;
		case "皮卡":
			int[] ladenR = {2,4,6};   //重新定义数组ladenR：载客量（人）
			int[] ladenT = {3,6,10};  //重新定义数组ladenT：载货量（吨）
			switch (xType) {
			case "小型":
			    xLadenR = ladenR[0];
			    xLadenT = ladenT[0];
				System.out.println("小型皮卡可【载客"+xLadenR+"人，载货"+xLadenT+"吨】");
				break;
			case "中型":
			    xLadenR = ladenR[1];
                xLadenT = ladenT[1];
				System.out.println("中型皮卡可【载客"+xLadenR+"人，载货"+xLadenT+"吨】");
				break;
			case "大型":
			    //xLadenR = ladenR[2];
                //xLadenT = ladenT[2];
				//System.out.print("大型皮卡可【载客"+xLadenT+"人，载货"+xLadenR+"吨】");
				System.out.println("对不起，暂无【大型】皮卡，请选择其他型号皮卡吧！");
				break;
			}break;
		}
		//========================================
		
		//打印可选颜色
		System.out.print("请选择您要的颜色（ ");
		for (int i=0; i<color.length; i++) {
			System.out.print(i+"、"+color[i]+" ");
		}
		System.out.print("）：");
		//输入颜色信息
		x = input.nextInt();
		xColor = color[x];
		//========================================
		
		//打印最终选择的车型信息
		System.out.println("您最终选择的车型信息为："+xModel+" "+xBrand+" "+xType+" "+xColor+" 载货量"+xLadenT+"吨 载客量"+xLadenR+"人");
		System.out.println("");
	}

}