package com.cnsdhh.car;

import java.util.Scanner;

public class SumPrice extends Car {
	
	//声明成员变量
	int day;       //租期天数
	float priceD;  //每天费用（元）	
	float sum;     //总费用

	//实现抽象方法
	@Override
	public void sumPrice() {
		// TODO 自动生成的方法存根
		
		//创建SelectCar对象：car
		SelectCar car = new SelectCar();
		car.selectCar();
		
		//打印可选天数
		System.out.print("请输入您要租用的天数（1 ~ 365）：");
		//输入天数信息
		Scanner input = new Scanner(System.in);
		day = input.nextInt();    //接收Int类型数据
		if (day>=1 && day<=360) {
			System.out.print("您最终的费用为：");
		} else {
			System.out.println("输入的天数有误，请重新输入！");
		}
		//========================================
		
		switch (car.xModel) {
		case "货车":
			switch (car.xType) {
			case "小型":
				switch (car.xLadenT) {
				case 2:
					priceD = 100;
					sum = car.xLadenT * day * priceD;
					break;
				case 4:
					priceD = 150;
					sum = car.xLadenT * day * priceD;
					break;
				}
				break;
			case "中型":
				switch (car.xLadenT) {
				case 6:
					priceD = 200;
					sum = car.xLadenT * day * priceD;
					break;
				case 8:
					priceD = 250;
					sum = car.xLadenT * day * priceD;
					break;
				case 10:
					priceD = 300;
					sum = car.xLadenT * day * priceD;
					break;
				}
				break;
			case "大型":
				switch (car.xLadenT) {
				case 20:
					priceD = 400;
					sum = car.xLadenT * day * priceD;
					break;
				case 40:
					priceD = 600;
					sum = car.xLadenT * day * priceD;
					break;
				}
				break;
			}
			break;
		case "客车":
			switch (car.xType) {
			case "小型":
				switch (car.xLadenR) {
				case 2:
					priceD = 150;
					sum = car.xLadenR * day * priceD;
					break;
				case 5:
					priceD = 300;
					sum = car.xLadenR * day * priceD;
					break;
				}
				break;
			case "中型":
				switch (car.xLadenR) {
				case 7:
					priceD = 450;
					sum = car.xLadenR * day * priceD;
					break;
				case 10:
					priceD = 550;
					sum = car.xLadenR * day * priceD;
					break;
				}
				break;
			case "大型":
				switch (car.xLadenR) {
				case 40:
					priceD = 800;
					sum = car.xLadenR * day * priceD;
					break;
				case 100:
					priceD = 1500;
					sum = car.xLadenR * day * priceD;
					break;
				}
				break;
			}
			break;
		case "皮卡":
			switch (car.xType) {
			case "小型":
				priceD = 200;
				sum = day * priceD;
				break;
			case "中型":
				priceD = 400;
				sum = day * priceD;
				break;
			case "大型":
				priceD = 600;
				sum = day * priceD;
			}
			break;
		}
		System.out.println(day+"天 * "+priceD+"元 = "+sum);

	}

}